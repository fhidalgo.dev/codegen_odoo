**Codegen Genérico**

Este script python permite que los diagramas de clase generados por la herramienta GTK+ de propósito general y multiplataforma para la creación de diagramas [Dia](https://wiki.gnome.org/Apps/Dia/), pueda ser exportado a código de un módulo de Odoo con la siguiente estructura:

Module/
*  \__init\__.py
*  \__manifest\__.py
*  README.rst
*  models/
    *  \__init\__.py
    *  model.py
*  views/
    *  view.xml
*  security/
    *  ir.model.access.csv
*  static/
    *  description/index.html

Fork del repositorio en github: [Github/Codegen](https://github.com/vijoin/codegen-odoo/tree/odoo10)

**Mejoras:**

1.  Todos los menús se generan al final de la vista.
2.  Se crea el archivo index.html para la descripción del módulo basado en la estrcutura de la OCA.
3.  El nombre de la clase sigue el estándar de una clase en Python, ejemplo: Cuándo se crea una clase en dia se define así: "nombre\_clase", y en la anterior versión del codegen cuando se exportaba el nombre de la clase era el mismo. Ahora lo que hace es que ese "nombre_clase", lo convierte (solo para el nombramiento de las clases) en: "NombreClase". Con ellos se logra seguir normas y estándares PEP8.
4.  Vistas Documentadas.
5.  El action de las vistas ahora toma el nombre de su clase correspondiente, ejemplo: "nombre_clase", en la vista: "Nombre Clase". Con su estética correspondiente.
6.  Cumple con los espaciados correspondientes entre clases y funciones.
7.  Reemplaza las "," de los valores de los campos por ",(espacio)". Esto permite que cuando se exporte no aparezcan errores de espaciado según las normas PEP8.
8.  Cada operación de clase puede ser definida con el nombre de la función, los parámetros que toma y el tipo de @api que corresponde. Además agrega automáticamente el parámetro self.
9.  El comentario de las clases se convierte en el atributo _description de la clase python.
10. El esteriotipo de la clase se convierte en los nombres de menús de la clase.
11. Agrega un menú principal con el nombre del módulo y de allí heredan las clases, esto para evitar lo del codegen anterior y su desorden de menús. Además cada menú genera un sequence autoincremental de 10 en 10.
12. Se quitaron los # -*- coding: utf-8 -*- de los archivos .py generados
13. Se agregó el archivo README.rst
14. Se quitó el atributo string de las etiquetas form y tree de las vistas
15. Se colocó el atributo multi_edit="1" en la vista tree
16. Se agregó la etiqueta `<sheet></sheet>` a la vista formulario
17. Se agregó la etiqueta `<header></header>` a la vista formulario
18. Se agregó la clase `<div class="oe_button_box" name="button_box"/>` en la vista formulario
19. Se agregó una vista search solo con búsquedas predictivas
20. Se colocó el atributo sample="1" en la vista tree

**Developers:**

1.  Franyer Hidalgo VE
