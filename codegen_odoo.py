# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import os
import zipfile
import dia

#
# This code is inspired by codegen.py
#


class Klass:
    def __init__(self, name):
        self.name = name
        self.attributes = []
        # a list, as java/c++ support multiple methods with the same name
        self.stereotype = ""
        self.operations = []
        self.comment = ""
        self.parents = []
        self.templates = []
        self.inheritance_type = ""

    def AddAttribute(self, name, type, visibility, value, comment):
        self.attributes.append((name, (type, visibility, value, comment)))

    def AddOperation(self, name, type, visibility, params, inheritance_type, comment, class_scope):
        self.operations.append((name, (type, visibility, params, inheritance_type, comment, class_scope)))

    def SetComment(self, s):
        self.comment = s

    def AddParent(self, parent):
        self.parents.append(parent)

    def AddTemplate(self, template):
        self.templates.append(template)

    def SetInheritance_type(self, inheritance_type):
        self.inheritance_type = inheritance_type


class ObjRenderer:
    "Implements the Object Renderer Interface and transforms diagram into its internal representation"

    def __init__(self):
        # an empty dictionary of classes
        self.klasses = {}
        self.klass_names = []   # store class names to maintain order
        self.arrows = []
        self.filename = ""

    def begin_render(self, data, filename):
        self.filename = filename
        for layer in data.layers:
            # for the moment ignore layer info. But we could use this to spread accross different files
            for o in layer.objects:
                if o.type.name == "UML - Class":

                    k = Klass(o.properties["name"].value)
                    k.SetComment(o.properties["comment"].value)
                    k.stereotype = o.properties["stereotype"].value
                    if o.properties["abstract"].value:
                        k.SetInheritance_type("abstract")
                    if o.properties["template"].value:
                        k.SetInheritance_type("template")
                    for op in o.properties["operations"].value:
                        # op : a tuple with fixed placing, see: objects/UML/umloperations.c:umloperation_props
                        # (name, type, comment, stereotype, visibility, inheritance_type, class_scope, params)
                        params = []
                        for par in op[8]:
                            # par : again fixed placement, see objects/UML/umlparameter.c:umlparameter_props
                            params.append((par[0], par[1]))
                        k.AddOperation(op[0], op[1], op[4], params, op[5], op[2], op[7])
                    # print o.properties["attributes"].value
                    for attr in o.properties["attributes"].value:
                        # see objects/UML/umlattributes.c:umlattribute_props
                        # print "    ", attr[0], attr[1], attr[4]
                        k.AddAttribute(attr[0], attr[1], attr[4], attr[2], attr[3])
                    self.klasses[o.properties["name"].value] = k
                    self.klass_names += [o.properties["name"].value]
                    # Connections
                elif o.type.name == "UML - Association":
                    # should already have got attributes relation by names
                    pass
                # other UML objects which may be interesting
                # UML - Note, UML - LargePackage, UML - SmallPackage, UML - Dependency, ...

        edges = {}
        for layer in data.layers:
            for o in layer.objects:
                for c in o.connections:
                    for n in c.connected:
                        if not n.type.name in ("UML - Generalization", "UML - Realizes"):
                            continue
                        if str(n) in edges:
                            continue
                        edges[str(n)] = None
                        if not (n.handles[0].connected_to and n.handles[1].connected_to):
                            continue
                        par = n.handles[0].connected_to.object
                        chi = n.handles[1].connected_to.object
                        if not par.type.name == "UML - Class" and chi.type.name == "UML - Class":
                            continue
                        par_name = par.properties["name"].value
                        chi_name = chi.properties["name"].value
                        if n.type.name == "UML - Generalization":
                            self.klasses[chi_name].AddParent(par_name)
                        else:
                            self.klasses[chi_name].AddTemplate(par_name)

    def end_render(self):
        # without this we would accumulate info from every pass
        self.attributes = []
        self.operations = {}


class OpenERPRenderer(ObjRenderer):
    def __init__(self):
        ObjRenderer.__init__(self)

    def data_get(self):
        return {
            'file': self.filename,
            'module': os.path.basename(self.filename).split('.')[-2]
        }

    def terp_get(self):
        data = {
            'module': self.data_get().get("module"),
            'name_menu': self.data_get().get("module").replace('_', ' ').title(),
            # menu_id = name_module.lower()
        }
        terp = '''{
    "name": "%(name_menu)s",
    "version": "16.0.0.0.0",
    "author": "Autor",
    "contributors": [],
    "maintainer": "Mantenedor",
    "website": "",
    "images": [],
    "category": "Services",
    "license": "LGPL-3",
    "summary": "",
    "depends": ["base"],
    "data": ["security/ir.model.access.csv",
             "views/%(module)s_views.xml", ],
    "assets": {},
    "external_dependencies": {},
    "demo": [],
    "css": [],
    "qweb": [],
    "installable": True,
    "auto_install": False,
    "application": True,
}\n''' % data
        return terp

    def readme_rst_get(self):
        name_menu = self.data_get().get("module").replace('_', ' ').title()
        readme = '''======================================
{}
======================================

.. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !! This file is generated by oca-gen-addon-readme !!
   !! changes will be overwritten.                   !!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

.. |badge1| image:: https://img.shields.io/badge/maturity-Alpha-green.png
    :target: https://odoo-community.org/page/development-status
    :alt: Alpha
.. |badge2| image:: https://img.shields.io/badge/licence-AGPL--3-blue.png
    :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
    :alt: License: AGPL-3
.. |badge3| image:: https://img.shields.io/badge/gitlab-OCA%2Frepo-name-lightgray.png?logo=gitlab
    :target: url_git
    :alt: name_group/name_repo

|badge1| |badge2| |badge3|

{}

  * Description ...

**Table of contents**

.. contents::
   :local:

Installation
============

Usage
=====

Usage ...

Bug Tracker
===========

Bugs are tracked on `GitLab Issues <issue git>`_.
In case of trouble, please check there if your issue has already been reported.
If you spotted it first, help us smashing it by providing a detailed and welcomed
`feedback <https://gitlab.com>`_.

Do not contact contributors directly about support or help with technical issues.

Credits
=======

Authors
~~~~~~~

* Author name

Contributors
~~~~~~~~~~~~

* Contributor <email>\n'''.format(name_menu, name_menu)
        return readme

    def html_get(self):
        return """<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.15.1: http://docutils.sourceforge.net/" />
<title>Nombre del Módulo</title>
<style type="text/css">

/*
:Author: David Goodger (goodger@python.org)
:Id: $Id: html4css1.css 7952 2016-07-26 18:15:59Z milde $
:Copyright: This stylesheet has been placed in the public domain.
Default cascading style sheet for the HTML output of Docutils.
See http://docutils.sf.net/docs/howto/html-stylesheets.html for how to
customize this style sheet.
*/

/* used to remove borders from tables and images */
.borderless, table.borderless td, table.borderless th {
  border: 0 }

table.borderless td, table.borderless th {
  /* Override padding for "table.docutils td" with "! important".
     The right padding separates the table cells. */
  padding: 0 0.5em 0 0 ! important }

.first {
  /* Override more specific margin styles with "! important". */
  margin-top: 0 ! important }

.last, .with-subtitle {
  margin-bottom: 0 ! important }

.hidden {
  display: none }

.subscript {
  vertical-align: sub;
  font-size: smaller }

.superscript {
  vertical-align: super;
  font-size: smaller }

a.toc-backref {
  text-decoration: none ;
  color: black }

blockquote.epigraph {
  margin: 2em 5em ; }

dl.docutils dd {
  margin-bottom: 0.5em }

object[type="image/svg+xml"], object[type="application/x-shockwave-flash"] {
  overflow: hidden;
}

/* Uncomment (and remove this text!) to get bold-faced definition list terms
dl.docutils dt {
  font-weight: bold }
*/

div.abstract {
  margin: 2em 5em }

div.abstract p.topic-title {
  font-weight: bold ;
  text-align: center }

div.admonition, div.attention, div.caution, div.danger, div.error,
div.hint, div.important, div.note, div.tip, div.warning {
  margin: 2em ;
  border: medium outset ;
  padding: 1em }

div.admonition p.admonition-title, div.hint p.admonition-title,
div.important p.admonition-title, div.note p.admonition-title,
div.tip p.admonition-title {
  font-weight: bold ;
  font-family: sans-serif }

div.attention p.admonition-title, div.caution p.admonition-title,
div.danger p.admonition-title, div.error p.admonition-title,
div.warning p.admonition-title, .code .error {
  color: red ;
  font-weight: bold ;
  font-family: sans-serif }

/* Uncomment (and remove this text!) to get reduced vertical space in
   compound paragraphs.
div.compound .compound-first, div.compound .compound-middle {
  margin-bottom: 0.5em }
div.compound .compound-last, div.compound .compound-middle {
  margin-top: 0.5em }
*/

div.dedication {
  margin: 2em 5em ;
  text-align: center ;
  font-style: italic }

div.dedication p.topic-title {
  font-weight: bold ;
  font-style: normal }

div.figure {
  margin-left: 2em ;
  margin-right: 2em }

div.footer, div.header {
  clear: both;
  font-size: smaller }

div.line-block {
  display: block ;
  margin-top: 1em ;
  margin-bottom: 1em }

div.line-block div.line-block {
  margin-top: 0 ;
  margin-bottom: 0 ;
  margin-left: 1.5em }

div.sidebar {
  margin: 0 0 0.5em 1em ;
  border: medium outset ;
  padding: 1em ;
  background-color: #ffffee ;
  width: 40% ;
  float: right ;
  clear: right }

div.sidebar p.rubric {
  font-family: sans-serif ;
  font-size: medium }

div.system-messages {
  margin: 5em }

div.system-messages h1 {
  color: red }

div.system-message {
  border: medium outset ;
  padding: 1em }

div.system-message p.system-message-title {
  color: red ;
  font-weight: bold }

div.topic {
  margin: 2em }

h1.section-subtitle, h2.section-subtitle, h3.section-subtitle,
h4.section-subtitle, h5.section-subtitle, h6.section-subtitle {
  margin-top: 0.4em }

h1.title {
  text-align: center }

h2.subtitle {
  text-align: center }

hr.docutils {
  width: 75% }

img.align-left, .figure.align-left, object.align-left, table.align-left {
  clear: left ;
  float: left ;
  margin-right: 1em }

img.align-right, .figure.align-right, object.align-right, table.align-right {
  clear: right ;
  float: right ;
  margin-left: 1em }

img.align-center, .figure.align-center, object.align-center {
  display: block;
  margin-left: auto;
  margin-right: auto;
}

table.align-center {
  margin-left: auto;
  margin-right: auto;
}

.align-left {
  text-align: left }

.align-center {
  clear: both ;
  text-align: center }

.align-right {
  text-align: right }

/* reset inner alignment in figures */
div.align-right {
  text-align: inherit }

/* div.align-center * { */
/*   text-align: left } */

.align-top    {
  vertical-align: top }

.align-middle {
  vertical-align: middle }

.align-bottom {
  vertical-align: bottom }

ol.simple, ul.simple {
  margin-bottom: 1em }

ol.arabic {
  list-style: decimal }

ol.loweralpha {
  list-style: lower-alpha }

ol.upperalpha {
  list-style: upper-alpha }

ol.lowerroman {
  list-style: lower-roman }

ol.upperroman {
  list-style: upper-roman }

p.attribution {
  text-align: right ;
  margin-left: 50% }

p.caption {
  font-style: italic }

p.credits {
  font-style: italic ;
  font-size: smaller }

p.label {
  white-space: nowrap }

p.rubric {
  font-weight: bold ;
  font-size: larger ;
  color: maroon ;
  text-align: center }

p.sidebar-title {
  font-family: sans-serif ;
  font-weight: bold ;
  font-size: larger }

p.sidebar-subtitle {
  font-family: sans-serif ;
  font-weight: bold }

p.topic-title {
  font-weight: bold }

pre.address {
  margin-bottom: 0 ;
  margin-top: 0 ;
  font: inherit }

pre.literal-block, pre.doctest-block, pre.math, pre.code {
  margin-left: 2em ;
  margin-right: 2em }

pre.code .ln { color: grey; } /* line numbers */
pre.code, code { background-color: #eeeeee }
pre.code .comment, code .comment { color: #5C6576 }
pre.code .keyword, code .keyword { color: #3B0D06; font-weight: bold }
pre.code .literal.string, code .literal.string { color: #0C5404 }
pre.code .name.builtin, code .name.builtin { color: #352B84 }
pre.code .deleted, code .deleted { background-color: #DEB0A1}
pre.code .inserted, code .inserted { background-color: #A3D289}

span.classifier {
  font-family: sans-serif ;
  font-style: oblique }

span.classifier-delimiter {
  font-family: sans-serif ;
  font-weight: bold }

span.interpreted {
  font-family: sans-serif }

span.option {
  white-space: nowrap }

span.pre {
  white-space: pre }

span.problematic {
  color: red }

span.section-subtitle {
  /* font-size relative to parent (h1..h6 element) */
  font-size: 80% }

table.citation {
  border-left: solid 1px gray;
  margin-left: 1px }

table.docinfo {
  margin: 2em 4em }

table.docutils {
  margin-top: 0.5em ;
  margin-bottom: 0.5em }

table.footnote {
  border-left: solid 1px black;
  margin-left: 1px }

table.docutils td, table.docutils th,
table.docinfo td, table.docinfo th {
  padding-left: 0.5em ;
  padding-right: 0.5em ;
  vertical-align: top }

table.docutils th.field-name, table.docinfo th.docinfo-name {
  font-weight: bold ;
  text-align: left ;
  white-space: nowrap ;
  padding-left: 0 }

/* "booktabs" style (no vertical lines) */
table.docutils.booktabs {
  border: 0px;
  border-top: 2px solid;
  border-bottom: 2px solid;
  border-collapse: collapse;
}
table.docutils.booktabs * {
  border: 0px;
}
table.docutils.booktabs th {
  border-bottom: thin solid;
  text-align: left;
}

h1 tt.docutils, h2 tt.docutils, h3 tt.docutils,
h4 tt.docutils, h5 tt.docutils, h6 tt.docutils {
  font-size: 100% }

ul.auto-toc {
  list-style-type: none }

</style>
</head>
<body>
<div class="document" id="name-technical-module">
<h1 class="title">Nombre del Módulo</h1>

<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! This file is generated by oca-gen-addon-readme !!
!! changes will be overwritten.                   !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
<p>
  <a class="reference external" href="https://odoo-community.org/page/development-status">
      <img alt="Alpha" src="https://raster.shields.io/badge/maduración-Alpha-yellow.png" />
  </a>
  <a class="reference external" href="http://www.gnu.org/licenses/agpl-3.0-standalone.html">
      <img alt="Licencia: AGPL-3" src="https://raster.shields.io/badge/licencia-AGPL--3-blue.png" />
  </a>
  <a class="reference external" href="link_repositorio_git">
      <img alt="Repositorio/addons" src="https://raster.shields.io/badge/gitlab-Repositorio%2Faddons-lightgray.png?logo=gitlab" />
  </a>
  <a class="reference external" href="link_saas_video_etc">
      <img alt="Demo" src="https://raster.shields.io/badge/demo-Pruébame-875A7B.png" />
  </a>
</p>
<p>This module is <em>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus tristique quam, sed fermentum arcu laoreet ac. Donec vel neque eros.</em></p>
<p><strong>Table of contents</strong></p>
<div class="contents local topic" id="contents">
  <ul class="simple">
    <li><a class="reference internal" href="#usage" id="id1">Usage</a></li>
    <li><a class="reference internal" href="#bug-tracker" id="id2">Bug Tracker</a></li>
    <li><a class="reference internal" href="#credits" id="id3">Credits</a>
      <ul>
        <li><a class="reference internal" href="#authors" id="id4">Authors</a></li>
        <li><a class="reference internal" href="#contributors" id="id5">Contributors</a></li>
        <li><a class="reference internal" href="#maintainers" id="id6">Maintainers</a></li>
      </ul>
    </li>
  </ul>
</div>
<div class="section" id="usage">
  <h1><a class="toc-backref" href="#id1">Usage</a></h1>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus tristique quam, sed fermentum arcu laoreet ac. Donec vel neque eros.</p>
</div>
<div class="section" id="bug-tracker">
  <h1><a class="toc-backref" href="#id2">Bug Tracker</a></h1>
  <p>Bugs are tracked on <a class="reference external" href="https://gitlab.com/fhidalgo.dev/codegen_odoo/-/issues">Gitlab Issues</a>.
  In case of trouble, please check there if your issue has already been reported.
  If you spotted it first, help us smashing it by providing a detailed and welcomed
  <a class="reference external" href="https://gitlab.com/fhidalgo.dev/codegen_odoo/-/issues">feedback</a>.</p>
  <p>Do not contact contributors directly about support or help with technical issues.</p>
</div>
<div class="section" id="credits">
<h1><a class="toc-backref" href="#id3">Credits</a></h1>
<div class="section" id="authors">
  <h2><a class="toc-backref" href="#id4">Authors</a></h2>
  <ul class="simple">
  <li>Name Author</li>
  </ul>
</div>
<div class="section" id="contributors">
  <h2><a class="toc-backref" href="#id5">Contributors</a></h2>
  <ul class="simple">
  <li>Name Contributor &lt;<a class="reference external" href="mailto:mail&#64;example.com">mail&#64;example.com</a>&gt;</li>
  </ul>
</div>
<div class="section" id="maintainers">
  <h2><a class="toc-backref" href="#id6">Maintainers</a></h2>
  <p>This module is maintained by the name maintainer.</p>
  <a class="reference external image-reference" href="https://odoo-community.org"><img alt="Odoo Community Association" src="https://odoo-community.org/logo.png" /></a>
  <p>Organization summary</p>
  <p>You are welcome to contribute. To learn how please visit <a class="reference external" href="https://odoo-community.org/page/Contribute">https://odoo-community.org/page/Contribute</a>.</p>
</div>
</div>
</div>
</body>
</html>
    """

    def init_get(self):
        return """from . import models\n"""

    def security_get(self):
        header = """"id","name","model_id:id","group_id:id","perm_read","perm_write","perm_create","perm_unlink"\n"""

        rows = """"access_%s_group_user","%s.group.user","model_%s","base.group_user",1,1,1,1"""
        for clas in self.klass_names:
            cname = clas.replace('.', '_')
            header += rows % (cname, clas, cname) + '\n'
        return header

    def get_label(self, attrs):
        label = None
        if attrs[0] == 'Many2many':
            for att in attrs[2].split(','):
                if 'string' in att:
                    label = att[8:]
            if not label:
                label = attrs[2].split(',')[4]
        if attrs[0] == 'One2many':
            for att in attrs[2].split(','):
                if 'string' in att:
                    label = att[8:]
            if not label:
                label = attrs[2].split(',')[2]
        if attrs[0] == 'Text':
            for att in attrs[2].split(','):
                if 'string' in att:
                    label = att[8:]
            if not label:
                label = attrs[2].split(',')[0]
        return label

    def view_class_get(self, cn, cd):
        data = self.data_get()
        fields_form = fields_tree = ""
        cols = {}
        for sa, attr in cd.attributes:
            cols[sa] = True
            attrs = ''
            if attr[0] in ('One2many', 'Many2many', 'Text'):
                attrs = ' colspan="4" nolabel="1"'
                field_label = self.get_label(attr).replace('\'', '').replace('"', '')
                fields_form += ("                        <separator string=\"%s\" colspan=\"4\"/>\n") % (field_label or 'Unknown')
            fields_form += ("                        <field name=\"%s\"" + attrs + "/>\n") % (sa)
            if attr[0] not in ('One2many', 'Many2many'):
                fields_tree += "                <field name=\"%s\"/>\n" % (sa,)
        data['form'] = fields_form
        data['tree'] = fields_tree
        data['name_id'] = cn.replace('.', '_')
        if not cd.stereotype:
            data['menu'] = 'Unknown/ ' + cn.replace('.', '_')
        else:
            data['menu'] = cd.stereotype
        data['name'] = cn
        data['name_en'] = data['menu'].split('/')[-1]
        data['mode'] = 'tree,form'
        if 'date' in cols:
            data['mode'] = 'tree,form,calendar'
        result = """
    <!-- VIEW FORM: %(menu)s -->
    <record id="%(name_id)s_view_form" model="ir.ui.view">
        <field name="name">%(name)s.form</field>
        <field name="model">%(name)s</field>
        <field name="type">form</field>
        <field name="arch" type="xml">
            <form>
                <header>
                </header>
                <sheet>
                    <div class="oe_button_box" name="button_box"/>
                    <group col="4" colspan="2">
%(form)s
                    </group>
                </sheet>
            </form>
        </field>
    </record>
    <!-- END VIEW FORM: %(menu)s -->

    <!-- VIEW TREE: %(menu)s -->
    <record id="%(name_id)s_view_tree" model="ir.ui.view">
        <field name="name">%(name)s.tree</field>
        <field name="model">%(name)s</field>
        <field name="type">tree</field>
        <field name="arch" type="xml">
            <tree multi_edit="1" sample="1">
%(tree)s
            </tree>
        </field>
    </record>
    <!-- END VIEW TREE: %(menu)s -->

    <!-- VIEW SEARCH: %(menu)s -->
    <record id="%(name_id)s_view_search" model="ir.ui.view">
        <field name="name">%(name)s.search</field>
        <field name="model">%(name)s</field>
        <field name="arch" type="xml">
            <search>
%(tree)s
            </search>
        </field>
    </record>
    <!-- END VIEW SEARCH: %(menu)s -->

    <!-- ACTION: %(name_en)s -->
    <record id="%(name_id)s_action" model="ir.actions.act_window">
        <field name="name">%(name_en)s</field>
        <field name="res_model">%(name)s</field>
        <field name="view_mode">%(mode)s</field>
        <field name="help" type="html">
          <p class="oe_view_nocontent_create">
            Click to create a new record
          </p>
        </field>
    </record>
    <!-- END ACTION: %(name_en)s -->

        """ % data
        return result

    def get_menus(self, cn, cd, contador):
        data = self.data_get()
        data['parent'] = data.get("module").lower()
        data['name_id'] = cn.replace('.', '_')
        if not cd.stereotype:
            data['menu'] = 'Unknown/ ' + cn.replace('.', '_')
        else:
            data['menu'] = cd.stereotype
        data['name'] = cn
        data['sequence'] = contador
        result = """
        <menuitem id="%(name_id)s_menu"
                  name="%(menu)s"
                  action="%(name_id)s_action"
                  parent="%(parent)s_main_menu"
                  sequence="%(sequence)s"/>
        """ % data
        return result

    def view_get(self):
        name_module = self.data_get().get("module")
        name_menu = name_module.replace('_', ' ').title()
        menu_id = name_module.lower()
        result = """<?xml version="1.0" encoding="UTF-8"?>
<odoo>
"""
        for sk in self.klass_names:
            result += self.view_class_get(sk, self.klasses[sk])

        result += """
<!-- ====================== MENUS ====================== -->
    <menuitem name="{0}"
              id="{1}_main_menu"
              sequence="1"/>
        """.format(name_menu, menu_id)

        contador = 0
        for sk in self.klass_names:
            contador += 10
            result += self.get_menus(sk, self.klasses[sk], contador)
        result += """

</odoo>\n"""
        return result

    def init_model_get(self):
        return """from . import %(module)s\n""" % self.data_get()

    def code_get(self):
        result = """from odoo import api, fields, models\n\n
"""
        count = 0
        for sk in self.klass_names:
            cname = sk.replace('.', ' ').replace('_', ' ')
            cname = cname.title().replace(' ', '')
            result += "class %s(models.Model):\n" % (cname,)
            result += '    _name = "%s"\n' % (sk,)

            parents = self.klasses[sk].parents
            if parents:
                result += "    _inherit = '" + parents[0] + "'\n"
            templates = self.klasses[sk].templates
            if templates:
                result += "    _inherits = {'" + templates[0] + "':'" + templates[0] + "'}\n"

            result += '    # _rec_name = ""\n'
            result += '    # _order = ""\n'
            if self.klasses[sk].comment:
                result += '    _description = """{0}"""\n'.format(self.klasses[sk].comment)
            default = {}
            result += "\n"
            for sa, attr in self.klasses[sk].attributes:
                value = attr[2].replace(",", ", ").replace(",  ", ", ")
                if attr[3]:
                    value += ", help='%s'" % (attr[3].replace("'", " "),)
                attr_type = attr[0]
                result += "    %s = fields.%s(%s)\n" % (sa, attr_type, value)

            if default:
                result += '    _defaults = {'
                for d in default:
                    result += "        '%s':lambda *args: '%s'\n" % (d, default[d])
                result += '    }'

            for so, op in self.klasses[sk].operations:
                pars = "self"
                tipo = op[0]
                for p in op[2]:
                    pars = pars + ", " + p[0]
                result += "\n    %s" % (tipo)
                result += "\n    def %s(%s):\n" % (so, pars)
                if op[4]: result += "        \"\"\" %s \"\"\"\n" % op[4]
                result += "        # return \n"
            count += 1
            if count != len(self.klass_names):
                result += "\n\n"
        return result

    def end_render(self):
        module = self.data_get()['module']
        zip = zipfile.ZipFile(self.filename, 'w')
        filewrite = {'__init__.py': self.init_get(),
                     '__manifest__.py': self.terp_get(),
                     'README.rst': self.readme_rst_get(),
                     'models/__init__.py': self.init_model_get(),
                     'models/' + module + '.py': self.code_get(),
                     'views/' + module + '_views.xml': self.view_get(),
                     'security/ir.model.access.csv': self.security_get(),
                     'static/description/index.html': self.html_get(), }
        for name, datastr in filewrite.items():
            info = zipfile.ZipInfo(module + '/' + name)
            info.compress_type = zipfile.ZIP_DEFLATED
            info.external_attr = 2175008768
            zip.writestr(info, datastr)
        zip.close()
        ObjRenderer.end_render(self)

# dia-python keeps a reference to the renderer class and uses it on demand


dia.register_export("PyDia Generador de Código (Odoo 16)", "zip", OpenERPRenderer())
# Modificado en Venezuela por Franyer Hidalgo
